;*******************************************************************************
; Implements procedures that abstracts working with the file system.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")

;*******************************************************************************
; Returns a list of directories that exist in the given base-directory. If a
; glob pattern is passed, than only the directories that match with it will be
; returned. For further information of file globbing, see the system command
; `ls`, which executes the globbing.
;
; Parameters:
;   base-directory - The base directory search for the directories in.
;   glob-pattern - [optional, default: #f] The glob pattern to match the
;     directories against.
;
; Returns: A list of directories or false in case of an error.
(define (get-directory-list base-directory . args) (let (
    (ret -1)
    (glob-pattern "*")
    (file-port 0)
    (dir-list '())
    (tmp-script-path (utils/get-unique-file-path "." "tmp-script"))
    (tmp-output-path (utils/get-unique-file-path base-directory "tmp" "out"))
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Globbing pattern parameter passed.
    (set! glob-pattern (list-ref args 0))
    (if (and (not (string? glob-pattern)) (not (symbol? glob-pattern)))
      (set! glob-pattern "*")
    )
  ))

  ; Generate a script for retrieving the directories of the directory filtered
  ; by the globbing pattern.
  (set! file-port (open-output-file tmp-script-path))
  (if (not (eqv? file-port #f)) (begin
    (display (string-append
      "#!/bin/bash\n"
      "\n"
      "# Save the current directory.\n"
      "BACKUP_DIR=$(pwd)\n"
      "\n"
      "# Change to the directory of which the directories should be retrieved.\n"
      "cd \"" base-directory "\" || exit 1\n"
      "\n"
      "# Delete the output file if it exists.\n"
      "rm -f \"" tmp-output-path "\"\n"
      "touch \"" tmp-output-path "\"\n"
      "\n"
      "# Get the directories of the directory according to the file glob \n"
      "# pattern and write it to the output file.\n"
      "for i in " glob-pattern "; do\n"
      "  if [[ -d \"$i\" ]]; then\n"
      "    echo \"$i\" >> \"" tmp-output-path "\"\n"
      "  fi\n"
      "done\n"
      "\n"
      "# Go back to the former directory.\n"
      "cd \"$BACKUP_DIR\" || exit 1\n"
    ) file-port)
    (close-output-port file-port)

    ; Execute the script.
    (set! ret (utils/exec-on-bash tmp-script-path))
    (remove-file tmp-script-path)

    ; Retrieve the result of the script and return it.
    (if (= ret 0) (begin
      (set! dir-list (utils/file-content-to-list tmp-output-path))
      (remove-file tmp-output-path)
      dir-list ; Return the result.
    ) #f) ; Return false if executing of script failed.
  ) #f) ; Return false if open output file (script file) failed.
))

;*******************************************************************************
; Checks if the given directory exists. Two modes are supported:  
;
; 1. If only the directory parameter is passed, than the directory path given
;   as directory parameter will be checked for existance.  
;   > Note: Only directories will return true, if the path ends with a file,
;     false will be returned.
; 2. If also the second parameter, a globbing pattern, is passed, than it
;   will be checked if any directory, matching the globbing pattern, exists
;   in the directory given by the first parameter.
;
; Parameters:
;   dir - A directory path to check for existance or the base directory
;     for the glob-pattern to match against.
;   glob-pattern - [optional, default: #f] The globbing pattern that must match
;     for directories in the directory given as the first parameter to return
;     true.
;
; Returns: True if the directory exists, false otherwise.
(define (does-directory-exist? dir . args) (let (
    (sys-cmd "")
    (dir-list '())
    (base-directory dir)
    (glob-pattern #f)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Globbing pattern parameter passed.
    (set! glob-pattern (list-ref args 0))
  ))

  (if (and (not (eqv? glob-pattern #f)) (string? glob-pattern)) (begin
      (set! sys-cmd (format #f "ls -d ~a" glob-pattern))
    ) (begin ;else
      (set! sys-cmd (format #f "ls -d ~a/" (strip-directory dir)))
      (set! base-directory (directory dir))
    )
  )

  ; Retrieve the directory list by executing the system command.
  (set! dir-list (utils/sys-cmd-output-to-object-list sys-cmd base-directory))

  ; If list length is greater than zero (= found directory), return #t.
  (and (not (eqv? dir-list #f)) (> (length dir-list) 0))
))

;*******************************************************************************
; Creates the directories given in the path string.
;
; Parameters:
;   path - The path with the directories to create, this means parent
;     directories will also be created.
;
; Returns: True if the directories were created successfully, false otherwise.
(define (make-directory path) (let (
    (sys-cmd "")
  )

  (if (or (string? path) (symbol? path)) (begin
      (set! sys-cmd (format #f "mkdir --parents \"~a\"" path))
      (if (eqv? (utils/exec-on-bash sys-cmd) 0)
        #t ; successfully created the directory
        #f ; failed to create the directory
      )
    ) (begin ; else
      (display
        "Failed to create directory, because path is not a string or symbol!\n")
      #f
    )
  )
))

;*******************************************************************************
; Copy the file (or a list of files) to the destination, which must be a path
; to an existing file.
;
; Parameters:
;   source - The file or a list of files to copy. A valid file source or list
;     entry must be a string or symbol path to the file.
;   destination - The destination to copy the file(s) to. This parameter must be
;     a string or symbol path pointing to a directory.
;
; Returns: True if copied successfully, false otherwise.
(define (copy-file source destination) (let (
    (sys-cmd "")
    (validPars #f)
    (source-str "")
    (is-source-list #f)
  )
  ; Check if parameters are valid.
  (set! validPars (and
    (or (string? destination) (symbol? destination))
    (file-directory? destination) ; destination must be a directory!
  ))

  (if (and validPars (not (or (string? source) (symbol? source)))) (begin
    ; source is not a string or source, therefore it must be a non empty list to
    ; be a valid source parameter.
    (if (not (pair? source)) (set! validPars #f) (set! is-source-list #t))
  ))

  ; If the source parameter is a list, build the source string.
  (if is-source-list (begin
    ; source parameter is a list
    (for-each (lambda (src)
      ; Check the current source if valid.
      (if (not (or (string? src) (symbol? src))) (begin
        (set! validPars #f) ; Current source is not valid.
      ) (begin ; else
        (set! source-str (string-append source-str " " src))
      ))
    ) source)
  ) (begin
    ; source parameter is a string
    (set! source-str source)
  ))

  ; Execute the copy command if parameters are valid.
  (if validPars (begin
    (set! sys-cmd (format #f "cp \"~a\" \"~a\"" source-str destination))
    (if (eqv? (utils/exec-on-bash sys-cmd) 0)
      #t ; successfully copied the file
      #f ; failed to copy the file
    )
  ) (begin ; else
    (display "Failed to copy file, invalid parameters!\n")
    #f
  ))
))

;*******************************************************************************
; Rename source to target (file or directory).
;
; Parameters:
;   source - The source to rename, must be a string or symbol.
;   target - The target name, must be a string or symbol.
;
; Returns: True if successfully renamed, false otherwise.
(define (rename-file-or-dir source target) (let (
    (sys-cmd "")
    (validPars #f)
  )

  ; Check if parameters are valid.
  (set! validPars (and
    (or (string? source) (symbol? source))
    (or (string? target) (symbol? target))
  ))

  ; Execute the rename command if parameters are valid.
  (if validPars (begin
      (set! sys-cmd (format #f "mv \"~a\" \"~a\"" source target))
      (if (eqv? (utils/exec-on-bash sys-cmd) 0)
        #t ; successfully renamed the file
        #f ; failed to rename the file
      )
    ) (begin ; else
      (display "Failed to rename file, invalid parameters!\n")
      #f
    )
  )
))

;*******************************************************************************
; Creates a symbolic link for the target in the directory given.
;
; Parameters:
;   target - The link target file, must be a string or symbol.
;   directory - The directory to create the link in, must be a string or symbol.
;
; Returns: True if successfully created the symbolic link, false otherwise.
(define (create-symbolic-link target directory) (let (
    (sys-cmd "")
    (validPars #f)
  )

  ; Check if parameters are valid.
  (set! validPars (and
    (or (string? target) (symbol? target))
    (or (string? directory) (symbol? directory))
  ))

  ; Execute the creating symbolic link command if parameters are valid.
  (if validPars (begin
      (set! sys-cmd (format #f "ln -s \"~a\" \"~a\"" target directory))
      (if (eqv? (utils/exec-on-bash sys-cmd) 0)
        #t ; successfully created the symbolic link
        #f ; failed to create the symbolic link
      )
    ) (begin ; else
      (display "Failed to create symbolic link, invalid parameters!\n")
      #f
    )
  )
))

;*******************************************************************************
; Retrieves a list of files in the given directory. If the glob-pattern
; parameter is passed, than only files that match the glob-pattern are returned.
;
; Parameters:
;   dir - The directory to get the list of files of.
;   glob-pattern - [optional, default: #f] The globbing pattern the files in the
;     directory have to match against.
;
; Returns: The list of files in the directory, or false if failed.
(define (get-files-of-directory dir . args) (let (
    (ret -1)
    (glob-pattern "*")
    (file-port 0)
    (files-list '())
    (tmp-script-path (utils/get-unique-file-path "." "tmp-script"))
    (tmp-output-path (utils/get-unique-file-path dir "tmp" "out"))
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Globbing pattern parameter passed.
    (set! glob-pattern (list-ref args 0))
    (if (and (not (string? glob-pattern)) (not (symbol? glob-pattern)))
      (set! glob-pattern "*")
    )
  ))

  ; Generate a script for retrieving the files of the directory filtered by the
  ; globbing pattern.
  (set! file-port (open-output-file tmp-script-path))
  (if (not (eqv? file-port #f)) (begin
    (display (string-append
      "#!/bin/bash\n"
      "\n"
      "# Save the current directory.\n"
      "BACKUP_DIR=$(pwd)\n"
      "\n"
      "# Change to the directory of which the files should be retrieved.\n"
      "cd \"" dir "\" || exit 1\n"
      "\n"
      "# Delete the output file if it exists.\n"
      "rm -f \"" tmp-output-path "\"\n"
      "touch \"" tmp-output-path "\"\n"
      "\n"
      "# Get the files of the directory according to the file glob pattern and\n"
      "# write it to the output file.\n"
      "for i in " glob-pattern "; do\n"
      "  if [[ -f \"$i\" ]]; then\n"
      (if (string=? dir (directory tmp-script-path)) (string-append
        "    if [[ \"$i\" != \"" (strip-directory tmp-script-path) "\" ]]; then\n"
        "      if [[ \"$i\" != \"" (strip-directory tmp-output-path) "\" ]]; then\n"
        "        echo \"$i\" >> \"" tmp-output-path "\"\n"
        "      fi\n"
        "    fi\n"
      ) (string-append ; else
        "    if [[ \"$i\" != \"" (strip-directory tmp-output-path) "\" ]]; then\n"
        "      echo \"$i\" >> \"" tmp-output-path "\"\n"
        "    fi\n"
      ))
      "  fi\n"
      "done\n"
      "\n"
      "# Go back to the former directory.\n"
      "cd \"$BACKUP_DIR\" || exit 1\n"
    ) file-port)
    (close-output-port file-port)

    ; Execute the script.
    (set! ret (utils/exec-on-bash tmp-script-path))
    (remove-file tmp-script-path)

    ; Retrieve the result of the script and return it.
    (if (= ret 0) (begin
      (set! files-list (utils/file-content-to-list tmp-output-path))
      (remove-file tmp-output-path)
      files-list ; Return the result.
    ) #f) ; Return false if executing of script failed.
  ) #f) ; Return false if open output file (script file) failed.
))

;*******************************************************************************
; Checks if the file in the given directory, matching the glob-pattern, exists.
; Checks if the given file exists. Two modes are supported:  
;
; 1. If only the file path parameter is passed, than the file path will be
;   checked if it exists.
; 2. If also the second parameter, a globbing pattern, is passed, than it will
;   be checked if the file, matching the globbing pattern, exists in the
;   directory given by the first parameter.
;
; Parameters:
;   path - A file path to check for existance or the base directory in which to
;     match the files against the glob-pattern.
;   glob-pattern - [optional, default: #f] The globbing pattern that must match
;     for the files in the directory given as the first parameter to return
;     true.
;
; Returns: True if the file exists, false otherwise.
(define (does-file-exist? path glob-pattern) (let (
    (sys-cmd "")
    (file-list '())
    (base-directory path)
    (glob-pattern #f)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Globbing pattern parameter passed.
    (set! glob-pattern (list-ref args 0))
  ))

  (if (and (not (eqv? glob-pattern #f)) (string? glob-pattern)) (begin
      (set! sys-cmd (format #f "ls -p ~a | grep -v /" glob-pattern))
    ) (begin ;else
      (set! sys-cmd (format #f "ls -p ~a | grep -v /" (strip-directory path)))
      (set! base-directory (directory path))
    )
  )

  ; Retrieve the files list by executing the system command.
  (set! file-list (utils/sys-cmd-output-to-object-list sys-cmd base-directory))

  ; If list length is greater than zero (= found file), return #t.
  (and (not (eqv? file-list #f)) (> (length file-list) 0))
))

;*******************************************************************************
; Resolves the given path to an absolute path, symlinks will alse be resolved.
; This is achieved by calling the system command `readlink`.
;
; Parameters:
;   path - The path to be resolved.
;
; Returns: The resolved absolute path or false if failed.
(define (resolve-path path) (let (
    (result-list '())
  )

  ; Check if parameter is a string or symbol.
  (if (not (or (string? path) (symbol? path)))
    (error "Invalid parameter!" path)
  )

  ; Get the resolved path by executing the system command.
  (set! result-list (utils/sys-cmd-output-to-object-list
    (format #f "readlink -f \"~a\"" path)
  ))

  ; If result list is not false and is a list with a length greater than zero,
  ; than the path could be resolved, otherwise return false.
  (if (and (not (eqv? result-list #f)) (> (length result-list) 0))
    (list-ref result-list 0) ; Return the resolved path.
    #f ; Path could not be resolved, therefore return false.
  )
))

;*******************************************************************************
; Retrieve the current directory.
;
; Returns: The current directory or false if failed.
(define (get-current-directory) (let (
    (result-list '())
  )

  ; Get the current directory by executing the system command.
  (set! result-list (utils/sys-cmd-output-to-object-list "pwd"))

  ; If result list is not false and is a list with a length greater than zero,
  ; than the current directory could be retrieved, otherwise return false.
  (if (and (not (eqv? result-list #f)) (> (length result-list) 0))
    (list-ref result-list 0) ; Return the current directory.
    #f ; The current directory could not be retrieved, therefore return false.
  )
))

;*******************************************************************************
; This procedure loops through all directories and subdirectories starting at
; the given directory (dir parameter) and executes the given procedure (proc
; parameter). If a directory pattern is passed, than the procedure will only be
; executed in directories that match with the directory pattern. The procedure's
; maximum subdirectory depth is limited by the max-depth parameter. The
; directory and file patterns are globbing patterns, for further information
; what a globbing pattern is, see the following links:  
;
; + https://en.wikipedia.org/wiki/Glob_(programming)
; + https://en.wikipedia.org/wiki/Bash_(Unix_shell)#Brace_expansion 
;   (section: "Brace expansion")
;
; Parameters:
;   dir - The starting directory.
;   config - A list of key value pair configurations for each level, e.g.:  
;     ```scheme
;     (list
;       (list 1 <level 1 configuration>)
;       (list 2 <level 2 configuration>)
;       ...
;       (list '* <configuration for all other levels>)
;     )
;     ```
;     A level is a number, that defines the depth level. e.g.: The level for
;     the starting directory is 1, the level for a subdirectory of the starting
;     directory is 2, the level for the subsubdirectory of the starting
;     directory is 3 and so on. For each level a specific configuration can be
;     passed in the list. The special level configuration with the level `'*` is
;     for all levels that are not configured explicitly in the list. A level
;     configuration is as well a key value pair list, e.g.:  
;     ```scheme
;     (list
;       (list 'pre-proc <procedure>)
;       (list 'post-proc <procedure>)
;       (list 'dir-pattern <directory pattern>)
;       (list 'file-pattern <file pattern>)
;     )
;     ```
;     The keys have the following meaning:  
;     * `'pre-proc` - [optional] The procedure that will be called if the level
;       directory matches with the directory pattern (`'dir-pattern` key). It
;       will be called, **before** the subdirectories of the current level
;       directory are processed, with the following parameters and return value:  
;       -> `dir` - The absolute path to the level directory (which matches the
;         directory pattern) for this call.  
;       -> `files` - A list of file names in the level directory that match the
;         file pattern (`'file-pattern` key).  
;       -> `level` - The current depth level.  
;       -> `context` - The context variable.  
;       **Return value of the `'pre-proc`:**  
;       - If the procedure returns false, than the processing of the
;         subdirectories of the current level directory will be skipped.
;       - If the context is returned and if the context is a list and if
;         the first element of this list is false, than the subdirectories
;         of the current level directory will also be skipped.
;       - If the context or true is returned, than the processing of the
;         subdirectories of the current level directory (if there are any
;         matching left) will continue.
;       - If the context is returned, than it will be passed to the next
;         `'pre-proc` and/or `'post-proc` call. If the first element of the
;         context is false, than it will be deleted from the context and the
;         rest of it will be passed to the next `'pre-proc` and/or `'post-proc`
;         call.
;     * `'post-proc` - [optional] The procedure that will be called if the level
;       directory matches with the directory pattern (`'dir-pattern` key). It
;       will be called, **after** the subdirectories of the current level
;       directory are processed, with the following parameters and return value:  
;       -> `dir` - The absolute path to the level directory (which matches the
;         directory pattern) for this call.  
;       -> `files` - A list of file names in the level directory that match the
;         file pattern (`'file-pattern` key).  
;       -> `level` - The current depth level.  
;       -> `context` - The context variable.  
;       **Return value of the `'post-proc`:**  
;       - If the procedure returns false, than the `loop-over-directory-tree`
;         procedure stops looping over the directory tree and returns
;         immediately (but `'post-proc` procedures will be called if there are
;         any).
;       - If the context is returned and if the context is a list and if
;         the first element of this list is false, than the
;         `loop-over-directory-tree` procedure stops looping over the
;         directory tree and returns immediately (but `'post-proc` procedures
;         will be called if there are any).
;       - If the context or true is returned, than the processing will
;         continue with the next directory in the tree.
;       - If the context is returned, than it will be passed to the next
;         `'pre-proc` and/or `'post-proc` call. If the first element of the
;         context is false, than it will be deleted from the context.
;     * `'dir-pattern` - [optional, default: `"*"`] A globbing pattern the level
;       directory must match against. See `'pre-proc` and `'post-proc` keys.
;     * `'file-pattern` - [optional, default: `"*"`] A globbing pattern the file
;       names in the level directory must match against. See `'pre-proc` and 
;       `'post-proc` keys.  
;
;     The procedure will only continue with subdirectories of the level if the
;     directory pattern matches with the level directory.
;     If no specific level configurations are passed, than the special level
;     configuration `'*` is mandatory, otherwise it is optional.
;   context - [optional, default: '()] A context variable that will be passed to
;     every level procedure. Every level procedure can manipulate the context
;     and return the manipulated context. This manipulated context will than be
;     passed to the next level procedure.
;   max-depth - [optional, default: 20] The maximum depth of subdirectories the
;     procedure will cover, counting from the starting directory.
;
; Returns: The manipulated context.
(define (loop-over-directory-tree dir config . args) (letrec (
    (context '())
    (max-depth 20)
    (get-level-config (lambda (level config) (let (
        (level-config '())
        (special-config '())
      )
      (let loop ((remaining config))
        (cond
          ; Check if reached end of config list. If so, than a config for the
          ; given level could not be found, therefore take the special config.
          ((null? remaining) (set! level-config special-config))
          ; Check if the current config is the special config. If so, save it.
          ((eqv? (car (car remaining)) '*) (begin
            (set! special-config (car (cdr (car remaining))))
            (loop (cdr remaining)) ; Got to the next config entry.
          ))
          ; Check if current config is the one for the given level. If so, than
          ; return it.
          ((= (car (car remaining)) level)
            (set! level-config (car (cdr (car remaining))))
          )
          ; Got to the next config entry.
          (else (loop (cdr remaining)))
        )
      )
      ; Return the found level config or '() if not found (no special config
      ; also, this should not occur).
      level-config
    )))
  )
  ; Check directory parameter.
  (if (not (or (symbol? dir) (string? dir))) (error "Invalid parameter!" dir))
  (if (not (file-directory? dir)) (error "Directory does not exist!" dir))

  ; Check config parameter.
  (if (not (pair? config)) (error "Invalid parameter!" config))
  (if (< (length config) 1)
    (error (string-append
      "\"config\" parameter is invalid! When no specific level configuration "
      "is passed, than the special level configuration '* is mandatory."
    ) config)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; context parameter passed.
    (set! context (list-ref args 0))
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; max-depth parameter passed.
    (set! max-depth (list-ref args 1))
    ; Check if parameter is valid.
    (if (not (number? max-depth)) (error "Invalid parameter!" max-depth))
  ))

  ; Resolve base directory.
  (set! dir (resolve-path dir))

  ; Loop through tree.
  (let loop (
      (current-directory dir)
      (level 1)
      (level-config (get-level-config 1 config))
    ) (let (
      (object '())
      (dir-list '())
      (file-list '())
      (does-match #f)
      (continue-processing #t)
      (continue-with-subdirectories #t)
      (pre-proc (utils/map-get-value level-config 'pre-proc))
      (post-proc (utils/map-get-value level-config 'post-proc))
      (dir-pattern (utils/map-get-value level-config 'dir-pattern))
      (file-pattern (utils/map-get-value level-config 'file-pattern))
    )

    ; check patterns
    (if (eqv? dir-pattern 'key-not-found) (set! dir-pattern "*"))
    (if (eqv? file-pattern 'key-not-found) (set! file-pattern "*"))

    ; Only continue if directory pattern matches with the current-directory.
    (set! does-match (utils/does-string-match-glob?
      (strip-directory current-directory) dir-pattern))
    (if does-match (begin
      ; Get directory list of current directory.
      (set! dir-list (get-directory-list current-directory))
      (if (eqv? dir-list #f) (error (string-append
        "Could not retrieve directory list of directory \"" current-directory "\"!"
      )))

      ; Get files list of current directory.
      (set! file-list (get-files-of-directory current-directory file-pattern))
      (if (eqv? file-list #f) (error (string-append
        "Could not retrieve files list of directory \"" current-directory
        "\" with file pattern \"" file-pattern "\"!"
      )))

      ; Execute pre procedure if there is one.
      (if (not (eqv? pre-proc 'key-not-found)) (begin
        (set! object (pre-proc current-directory file-list level context))
        (if (boolean? object) (begin
          (set! continue-with-subdirectories object)
        ) (begin ; else
          (if (and (pair? object) (boolean? (car object))) (begin
            (set! context (cdr object))
            (set! continue-with-subdirectories (car object))
          ) (begin ; else
            (set! context object)
            (set! continue-with-subdirectories #t)
          ))
        ))
      ))

      ; Only continue processing with subdirectories if pre-proc returned true.
      ; If there was no pre-proc to call, than the processing will be continued
      ; anyway.
      (if continue-with-subdirectories (begin
        ; Only proceed with subdirectories if maximum level depth is not reached.
        (if (< level max-depth) (begin
          ; Proceed with subdirectories.
          (let dir-loop ((remaining-subdirs dir-list))
            (cond
              ; Check if reached end of subdirectories list.
              ((null? remaining-subdirs) (begin
                ; Return true, because processing should not stop.
                (set! continue-processing #t)
              ))
              ; Else process current subdirectory and than go to the next one.
              (else (begin
                (set! continue-processing (loop
                  (string-append current-directory "/" (car remaining-subdirs))
                  (+ level 1)
                  (get-level-config (+ level 1) config)
                ))

                ; Check if processing should continue and if so continue with
                ; next subdirectory.
                (if continue-processing (dir-loop (cdr remaining-subdirs)))
              ))
            )
          )
        ) (begin ; else
          ; Reached maximum depth, therefore do not read more subdirectories.
          ; Return true to proceed with other directories.
          (set! continue-processing #t)
        ))
      ))

      ; Execute post procedure if there is one.
      (if (not (eqv? post-proc 'key-not-found)) (begin
        (set! object (post-proc current-directory file-list level context))
        (if (boolean? object) (begin
          (set! continue-processing (and continue-processing object))
        ) (begin ; else
          (if (and (pair? object) (boolean? (car object))) (begin
            (set! context (cdr object))
            (set! continue-processing (and continue-processing (car object)))
          ) (begin ; else
            (set! context object)
            (set! continue-processing (and continue-processing #t))
          ))
        ))
      ))

      ; Return true if processing should continue, false otherwise.
      continue-processing
    ) (begin ; else
      ; Directory does not match with pattern.
      ; Return true, because processing should continue.
      #t
    ))
  ))

  context ; Return the context.
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'get-directory-list get-directory-list)
  (list 'does-directory-exist? does-directory-exist?)
  (list 'make-directory make-directory)
  (list 'copy-file copy-file)
  (list 'rename-file-or-dir rename-file-or-dir)
  (list 'create-symbolic-link create-symbolic-link)
  (list 'get-files-of-directory get-files-of-directory)
  (list 'does-file-exist? does-file-exist?)
  (list 'resolve-path resolve-path)
  (list 'get-current-directory get-current-directory)
  (list 'loop-over-directory-tree loop-over-directory-tree)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
