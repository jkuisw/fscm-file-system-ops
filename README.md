File System Operations Module
=============================

The `fscm-file-system-ops` module provides procedures for working with the file
system. For example creating and renaming of directories, renaming of files,
file path resolving, creating symbolic links, looping over the directory tree
and more. Below the installation and basic usage of this module is described, if
you are looking for the API documentation [click here](doc/README.md) (located
in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Installation
------------
Before you can install the file system operations module you have to properly 
setup the module manager. Follow the 
[Fluent Scheme Module Manager](https://gitlab.com/jkuisw/fscm-module-manager)
guide to set it up.

Now you need to clone the module repository to your computer. Choose a proper
location where you want to clone it, preferably the same directory where the
module manager repository is located (e.g.: `/home/<user name>/jkuisw`). Go into
that directory and clone the repository, as follows:

```shell
cd "/home/<user name>/jkuisw"
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-file-system-ops.git
```

After cloning the repository ensure that the `fscm-file-system-ops` module is 
registered to the module manager, as described in the 
[module manager setup](https://gitlab.com/jkuisw/fscm-module-manager#register-modules) 
(`add-module-paths` procedure).

Usage
-----

To use the file system operations module you have to import it with a proper 
import name. After that you can use the exported procedures of the module:

```scheme
; Import the file system operations module.
(import "fs" "fscm-file-system-ops")

; Using the file system operations module for example to get the list of 
; directories in the current directory that match the given glob pattern. 
; "a*" = all directories which begin with the letter "a"
(fs/get-directory-list "." "a*")
```

The full documentation for all procedures exported by the file system operations
module can be found in the [API documentation](doc/README.md). Below are only
some examples presented.  

### `loop-over-directory-tree` - Procedure
The most powerful procedure of the module is the `loop-over-directory-tree`. 
It's interface definition and description can be found in the 
[API documentation](doc/README.md), here we will outline the possibilities of
this procedure by an example.

#### Problem Definition
Consider the following directory structure (create it before executing the
example):

```
project-directory
  |--- dp0
  |  |--+ case-dir-0-0
  |  |--+ case-dir-0-1
  |  +--+ case-dir-0-2
  |--- dp1
  |  |--+ case-dir-1-0
  |  |--+ case-dir-1-1
  |  +--+ case-dir-1-2
  |--- dp2
  |  |--+ case-dir-2-0
  |  |--+ case-dir-2-1
  |  +--+ case-dir-2-2
  +--+ log
```

So you have a project directory with sub-directories for different design points,
each of them have different case directories and each case directory has some
files from which you want to collect data. In the end the collected data will
then be written to a file. Furthermore you want to write logging messages to a
logging file in a specific format with a timestamp to document the status of the
operation. The log file should be saved in the log folder. Also existing log
files should not be overwritten.

#### Solution
First we define the dependencies and import the modules.

```scheme
; Dependencies
(import "utils" "fscm-utils")
(import "fs" "fscm-file-system-ops")
```

Next we will define some procedures. The first one is the logging procedure
`logmsg`, which is responsible for logging messages to a log file opened by
another procedure. The logging procedure will create a time-stamp and write that
to the logging file followed by the logging message.

```scheme
(define (logmsg file-port message) (let (
    (curr-time (local-time (time)))
    (year 0) (month 0) (day 0)
    (hour 0) (minute 0) (second 0)
    (log-header "")
  )
  ; Extract time values.
  (set! year (+ (cdr (list-ref curr-time 3)) 1900))
  (set! month (+ (cdr (list-ref curr-time 4)) 1))
  (set! day (cdr (list-ref curr-time 5)))
  (set! hour (cdr (list-ref curr-time 6)))
  (set! minute (cdr (list-ref curr-time 7)))
  (set! second (cdr (list-ref curr-time 8)))

  ; Build log header.
  (set! log-header (format #f "[~02d.~02d.~04d ~02d:~02d:~02d] "
    day month year hour minute second
  ))

  ; Print the log message.
  (if (and (not (eqv? file-port 0)) (output-port? file-port)) (begin
    (display (string-append log-header message "\n") file-port)
  ) (begin ; else
    (display (string-append log-header message "\n"))
  ))
))
```

The second procedure is the `initialize-postprocessing`, which should be executed
before entering the project directory, to create and open the logging file and
do some other start-up stuff. Therefore it is registered later as `'pre-proc`
procedure (see the [API documentation](doc/README.md) for details) in the level
1 configuration. As you can see, the file port of the logging file will be saved
in the context. The context is a data object that will be passed to all
procedures so that they can all log to the same file. The following parameters
will be passed to the procedure:  
* `dir` - The current directory (depends on the level configuration).
* `files` - A list of files in the directory `dir` that match the file pattern
  of the level configuration where the `'pre-proc` was added.
* `level` - The level of the level configuration.
* `context` - The current context.

As you can see in the following code snippet, the path to the log file will be
created with the `utils/get-unique-file-path` command. This procedure ensures
that existing log files will not be overwritten by appending a number to the
file name.

```scheme
(define (initialize-postprocessing dir files level context) (let (
    (log-file-path (utils/get-unique-file-path
      (string-append dir "/log") "postprocessing" "log")
    )
    (log-port 0)
  )
  (set! log-port (open-output-file log-file-path))

  (logmsg log-port "#####################################################################")
  (logmsg log-port "Start postprocessing...")
  (logmsg log-port "#####################################################################")

  (utils/map-set-value context 'log-file-port log-port)

  context ; Return the context.
))
```

The third procedure is the `finish-postprocessing`, which should be executed at
the end, to write the collected data to a file and closing the log port.
Therefore it is registered later as `'post-proc` procedure (see the
[API documentation](doc/README.md) for details) in the level 1 configuration.
The following parameters will be passed to the procedure:  
* `dir` - The current directory (depends on the level configuration).
* `files` - A list of files in the directory `dir` that match the file pattern
  of the level configuration where the `'pre-proc` was added.
* `level` - The level of the level configuration.
* `context` - The current context.

```scheme
(define (finish-postprocessing dir files level context) (let (
    (file-port 0)
    (log-port (utils/map-get-value context 'log-file-port))
    (collected-data (utils/map-get-value context 'data))
  )
  ; Check if log file port exists.
  (if (eqv? log-port #f) (set! log-port 0))

  ; Write collected data to some file.
  (logmsg log-port "Writing collected data to some file...\n")
  
  ; Log that we are finished.
  (logmsg log-port "Finished postprocessing.")
  (logmsg log-port "---------------------------------------------------------------------")

  (close-output-port log-port)
  (utils/map-set-value context 'log-file-port 0)
  context ; Return the context.
))
```

The fourth procedure is the `enter-design-point`, which will be called every time
before a design point directory will be entered, which is the level 2. Therefore 
it is registered later as `'pre-proc` procedure (see the
[API documentation](doc/README.md) for details) in the level 2 configuration.
Because this is a `'pre-proc` procedure too, the parameters are the same as for
the `initialze-postprocessing` procedure.

```scheme
(define (enter-design-point dir files level context) (let (
    (log-port (utils/map-get-value context 'log-file-port))
  )
  ; Check if log file port exists.
  (if (eqv? log-port #f) (set! log-port 0))

  (logmsg log-port (string-append "Enter design point \"" (strip-directory dir) "\"."))

  #t ; Return true to continue processing.
))
```

The fifth procedure is the `case-dir-proc` which will be called every time
before a case directory will be entered, which is the level 3. Therefore it is 
registered later as `'pre-proc` procedure (see the
[API documentation](doc/README.md) for details) in the level 3 configuration.
Because this is a `'pre-proc` procedure too, the parameters are the same as for
the `initialze-postprocessing` procedure.

```scheme
(define (case-dir-proc dir files level context) (let (
    (log-port (utils/map-get-value context 'log-file-port))
    (data (utils/map-get-value context 'data))
  )
  ; Check if log file port exists.
  (if (eqv? log-port #f) (set! log-port 0))

  ; Collect data from the cases in the current directory.
  (logmsg log-port "Collecting data from the cases in the current directory...")
  
  ; Update the data in the context.
  (utils/map-set-value context 'data data)

  ; Return the context.
  context
))
```

The next procedure now defines the initial context and the configuration for
the `loop-over-directory-tree` command and calls the command with the context, 
configuration, the path to the project directory and a maximum depth of 3.

```scheme
(define (loop-through-cases project-directory) (let (
    (config '())
    (context (list
      (list 'data '())
      (list 'log-file-port 0)
    ))
  )
  ; Configure looping through project directory tree.
  (set! config (list
    (list 1 (list ; level 1 - project directory
      (list 'pre-proc initialize-postprocessing)
      (list 'post-proc finish-postprocessing)
    ))
    (list 2 (list ; level 2 - design point directory
      (list 'pre-proc enter-design-point)
      (list 'dir-pattern "dp?") ; directory pattern for level 2
    ))
    (list 3 (list ; level 3 - case directory
      (list 'pre-proc case-dir-proc)
      (list 'dir-pattern "*?-?") ; directory pattern for level 3
      (list 'file-pattern "*.dat.gz") ; file pattern for level 3
    ))
  ))

  ; Loop through project directory tree.
  (set! context (fs/loop-over-directory-tree project-directory config context 3))

  #t ; Return true for success.
))
```

As you can see the `initialize-postprocessing` procedure is registered as the
`'pre-proc` of the level 1 configuration and as the `'post-proc` the
`finish-postprocessing` procedure is registered. The level 1 is the project
directory level of the directory tree and has no directory or file patterns. In
the level 2 only a `'post-proc`, the `enter-design-point` procedure, is
registered and no `'pre-proc`. There is also a directory pattern configured,
which means that only the directories in the project-directory which match the
pattern will be considered (in oure example: "dp0", "dp1" and "dp2"). The level
2 is the design point directories level of the directory tree. Likewise in the
level 3 configuration only a `'post-proc`, the `case-dir-proc` procedure, is
registered and no `'pre-proc`. There is also a directory pattern configured,
which means that only the directories in the design point directories which
match the pattern will be considered. Furthermore a file pattern is defined, so
only the files in the case directories that match the pattern will be passed to
the `case-dir-proc`. To test the example, just call the `loop-through-cases`
procedure with the absolute path to the project-directory as first parameter:

```scheme
(loop-through-cases "/path/to/project/directory")
```

























<!-- -->
