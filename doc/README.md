# Module: fscm-file-system-ops
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: file-io](/doc/file-io.md) | Provides procedures for file input/output operations. |
| [:page_facing_up: file-system-operations](/doc/file-system-operations.md) | Implements procedures that abstracts working with the file system. |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the internal procedure names. Normally this is the same as the exported name, prepended with the import name, but thy can differ. Look into the detailed procedure documentation to get the export name of the procedure. Exported procedures are imported into a source file with the `import` procedure (see the fscm-module-manager module for further details), which takes the import name as the first parameter. Therefore every exported procedure of a module will be imported according to the following syntax:  
`<import name>/<export name of the procedure>`.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: read-line](/doc/file-io.md#read-line) | Reads one line from a file and returns it as case sensitive string. |
| [:page_facing_up: get-directory-list](/doc/file-system-operations.md#get-directory-list) | Returns a list of directories that exist in the given base-directory. If agl... |
| [:page_facing_up: does-directory-exist?](/doc/file-system-operations.md#does-directory-exist) | Checks if the given directory exists. Two modes are supported:  1. If only t... |
| [:page_facing_up: loop-over-directory-tree](/doc/file-system-operations.md#loop-over-directory-tree) | This procedure loops through all directories and subdirectories starting att... |
| [:page_facing_up: make-directory](/doc/file-system-operations.md#make-directory) | Creates the directories given in the path string. |
| [:page_facing_up: copy-file](/doc/file-system-operations.md#copy-file) | Copy the file (or a list of files) to the destination, which must be a patht... |
| [:page_facing_up: rename-file-or-dir](/doc/file-system-operations.md#rename-file-or-dir) | Rename source to target (file or directory). |
| [:page_facing_up: create-symbolic-link](/doc/file-system-operations.md#create-symbolic-link) | Creates a symbolic link for the target in the directory given. |
| [:page_facing_up: get-files-of-directory](/doc/file-system-operations.md#get-files-of-directory) | Retrieves a list of files in the given directory. If the glob-patternparamet... |
| [:page_facing_up: does-file-exist?](/doc/file-system-operations.md#does-file-exist) | Checks if the file in the given directory, matching the glob-pattern, exists... |
| [:page_facing_up: resolve-path](/doc/file-system-operations.md#resolve-path) | Resolves the given path to an absolute path, symlinks will alse be resolved.... |
| [:page_facing_up: get-current-directory](/doc/file-system-operations.md#get-current-directory) | Retrieve the current directory. |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

  
The module does not have any private procedures.  
  

