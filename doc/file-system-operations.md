:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: file-system-operations.scm
Implements procedures that abstracts working with the file system.

:arrow_right: [Go to source code of this file.](/src/file-system-operations.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: get-directory-list](#get-directory-list) | Returns a list of directories that exist in the given base-directory. If agl... |
| [:page_facing_up: does-directory-exist?](#does-directory-exist) | Checks if the given directory exists. Two modes are supported: 1. If only t... |
| [:page_facing_up: loop-over-directory-tree](#loop-over-directory-tree) | This procedure loops through all directories and subdirectories starting att... |
| [:page_facing_up: make-directory](#make-directory) | Creates the directories given in the path string. |
| [:page_facing_up: copy-file](#copy-file) | Copy the file (or a list of files) to the destination, which must be a patht... |
| [:page_facing_up: rename-file-or-dir](#rename-file-or-dir) | Rename source to target (file or directory). |
| [:page_facing_up: create-symbolic-link](#create-symbolic-link) | Creates a symbolic link for the target in the directory given. |
| [:page_facing_up: get-files-of-directory](#get-files-of-directory) | Retrieves a list of files in the given directory. If the glob-patternparamet... |
| [:page_facing_up: does-file-exist?](#does-file-exist) | Checks if the file in the given directory, matching the glob-pattern, exists... |
| [:page_facing_up: resolve-path](#resolve-path) | Resolves the given path to an absolute path, symlinks will alse be resolved.... |
| [:page_facing_up: get-current-directory](#get-current-directory) | Retrieve the current directory. |

## Procedure Documentation

### get-directory-list

#### Syntax
```scheme
(get-directory-list base-directory [glob-pattern])
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L20)

#### Export Name
`<import-name>/get-directory-list`

#### Description
Returns a list of directories that exist in the given base-directory. If a
glob pattern is passed, than only the directories that match with it will be
returned. For further information of file globbing, see the system command
`ls`, which executes the globbing.

#### Parameters
##### `base-directory`  
The base directory search for the directories in.

##### `glob-pattern`  
_Attributes: optional, default: `#f`_  
The glob pattern to match the
directories against.


#### Returns
A list of directories or false in case of an error.

-------------------------------------------------
### does-directory-exist?

#### Syntax
```scheme
(does-directory-exist? dir [glob-pattern])
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L100)

#### Export Name
`<import-name>/does-directory-exist?`

#### Description
Checks if the given directory exists. Two modes are supported:  

1. If only the directory parameter is passed, than the directory path given
  as directory parameter will be checked for existance.  
  > Note: Only directories will return true, if the path ends with a file,
    false will be returned.
2. If also the second parameter, a globbing pattern, is passed, than it
  will be checked if any directory, matching the globbing pattern, exists
  in the directory given by the first parameter.

#### Parameters
##### `dir`  
A directory path to check for existance or the base directory
for the glob-pattern to match against.

##### `glob-pattern`  
_Attributes: optional, default: `#f`_  
The globbing pattern that must match
for directories in the directory given as the first parameter to return
true.


#### Returns
True if the directory exists, false otherwise.

-------------------------------------------------
### loop-over-directory-tree

#### Syntax
```scheme
(loop-over-directory-tree dir config [context] [max-depth])
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L561)

#### Export Name
`<import-name>/loop-over-directory-tree`

#### Description
This procedure loops through all directories and subdirectories starting at
the given directory (dir parameter) and executes the given procedure (proc
parameter). If a directory pattern is passed, than the procedure will only be
executed in directories that match with the directory pattern. The procedure's
maximum subdirectory depth is limited by the max-depth parameter. The
directory and file patterns are globbing patterns, for further information
what a globbing pattern is, see the following links:  

+ https://en.wikipedia.org/wiki/Glob_(programming)
+ https://en.wikipedia.org/wiki/Bash_(Unix_shell)#Brace_expansion 
  (section: "Brace expansion")

#### Parameters
##### `dir`  
The starting directory.

##### `config`  
A list of key value pair configurations for each level, e.g.:  
```scheme
(list
  (list 1 <level 1 configuration>)
  (list 2 <level 2 configuration>)
  ...
  (list '* <configuration for all other levels>)
)
```
A level is a number, that defines the depth level. e.g.: The level for
the starting directory is 1, the level for a subdirectory of the starting
directory is 2, the level for the subsubdirectory of the starting
directory is 3 and so on. For each level a specific configuration can be
passed in the list. The special level configuration with the level `'*` is
for all levels that are not configured explicitly in the list. A level
configuration is as well a key value pair list, e.g.:  
```scheme
(list
  (list 'pre-proc <procedure>)
  (list 'post-proc <procedure>)
  (list 'dir-pattern <directory pattern>)
  (list 'file-pattern <file pattern>)
)
```
The keys have the following meaning:  
* `'pre-proc` - [optional] The procedure that will be called if the level
  directory matches with the directory pattern (`'dir-pattern` key). It
  will be called, **before** the subdirectories of the current level
  directory are processed, with the following parameters and return value:  
  -> `dir` - The absolute path to the level directory (which matches the
    directory pattern) for this call.  
  -> `files` - A list of file names in the level directory that match the
    file pattern (`'file-pattern` key).  
  -> `level` - The current depth level.  
  -> `context` - The context variable.  
  **Return value of the `'pre-proc`:**  
  - If the procedure returns false, than the processing of the
    subdirectories of the current level directory will be skipped.
  - If the context is returned and if the context is a list and if
    the first element of this list is false, than the subdirectories
    of the current level directory will also be skipped.
  - If the context or true is returned, than the processing of the
    subdirectories of the current level directory (if there are any
    matching left) will continue.
  - If the context is returned, than it will be passed to the next
    `'pre-proc` and/or `'post-proc` call. If the first element of the
    context is false, than it will be deleted from the context and the
    rest of it will be passed to the next `'pre-proc` and/or `'post-proc`
    call.
* `'post-proc` - [optional] The procedure that will be called if the level
  directory matches with the directory pattern (`'dir-pattern` key). It
  will be called, **after** the subdirectories of the current level
  directory are processed, with the following parameters and return value:  
  -> `dir` - The absolute path to the level directory (which matches the
    directory pattern) for this call.  
  -> `files` - A list of file names in the level directory that match the
    file pattern (`'file-pattern` key).  
  -> `level` - The current depth level.  
  -> `context` - The context variable.  
  **Return value of the `'post-proc`:**  
  - If the procedure returns false, than the `loop-over-directory-tree`
    procedure stops looping over the directory tree and returns
    immediately (but `'post-proc` procedures will be called if there are
    any).
  - If the context is returned and if the context is a list and if
    the first element of this list is false, than the
    `loop-over-directory-tree` procedure stops looping over the
    directory tree and returns immediately (but `'post-proc` procedures
    will be called if there are any).
  - If the context or true is returned, than the processing will
    continue with the next directory in the tree.
  - If the context is returned, than it will be passed to the next
    `'pre-proc` and/or `'post-proc` call. If the first element of the
    context is false, than it will be deleted from the context.
* `'dir-pattern` - [optional, default: `"*"`] A globbing pattern the level
  directory must match against. See `'pre-proc` and `'post-proc` keys.
* `'file-pattern` - [optional, default: `"*"`] A globbing pattern the file
  names in the level directory must match against. See `'pre-proc` and 
  `'post-proc` keys.  

The procedure will only continue with subdirectories of the level if the
directory pattern matches with the level directory.
If no specific level configurations are passed, than the special level
configuration `'*` is mandatory, otherwise it is optional.

##### `context`  
_Attributes: optional, default: `'()`_  
A context variable that will be passed to
every level procedure. Every level procedure can manipulate the context
and return the manipulated context. This manipulated context will than be
passed to the next level procedure.

##### `max-depth`  
_Attributes: optional, default: `20`_  
The maximum depth of subdirectories the
procedure will cover, counting from the starting directory.


#### Returns
The manipulated context.

-------------------------------------------------
### make-directory

#### Syntax
```scheme
(make-directory path)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L136)

#### Export Name
`<import-name>/make-directory`

#### Description
Creates the directories given in the path string.

#### Parameters
##### `path`  
The path with the directories to create, this means parent
directories will also be created.


#### Returns
True if the directories were created successfully, false otherwise.

-------------------------------------------------
### copy-file

#### Syntax
```scheme
(copy-file source destination)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L165)

#### Export Name
`<import-name>/copy-file`

#### Description
Copy the file (or a list of files) to the destination, which must be a path
to an existing file.

#### Parameters
##### `source`  
The file or a list of files to copy. A valid file source or list
entry must be a string or symbol path to the file.

##### `destination`  
The destination to copy the file(s) to. This parameter must be
a string or symbol path pointing to a directory.


#### Returns
True if copied successfully, false otherwise.

-------------------------------------------------
### rename-file-or-dir

#### Syntax
```scheme
(rename-file-or-dir source target)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L220)

#### Export Name
`<import-name>/rename-file-or-dir`

#### Description
Rename source to target (file or directory).

#### Parameters
##### `source`  
The source to rename, must be a string or symbol.

##### `target`  
The target name, must be a string or symbol.


#### Returns
True if successfully renamed, false otherwise.

-------------------------------------------------
### create-symbolic-link

#### Syntax
```scheme
(create-symbolic-link target directory)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L253)

#### Export Name
`<import-name>/create-symbolic-link`

#### Description
Creates a symbolic link for the target in the directory given.

#### Parameters
##### `target`  
The link target file, must be a string or symbol.

##### `directory`  
The directory to create the link in, must be a string or symbol.


#### Returns
True if successfully created the symbolic link, false otherwise.

-------------------------------------------------
### get-files-of-directory

#### Syntax
```scheme
(get-files-of-directory dir [glob-pattern])
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L288)

#### Export Name
`<import-name>/get-files-of-directory`

#### Description
Retrieves a list of files in the given directory. If the glob-pattern
parameter is passed, than only files that match the glob-pattern are returned.

#### Parameters
##### `dir`  
The directory to get the list of files of.

##### `glob-pattern`  
_Attributes: optional, default: `#f`_  
The globbing pattern the files in the
directory have to match against.


#### Returns
The list of files in the directory, or false if failed.

-------------------------------------------------
### does-file-exist?

#### Syntax
```scheme
(does-file-exist? path [glob-pattern])
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L377)

#### Export Name
`<import-name>/does-file-exist?`

#### Description
Checks if the file in the given directory, matching the glob-pattern, exists.
Checks if the given file exists. Two modes are supported:  

1. If only the file path parameter is passed, than the file path will be
  checked if it exists.
2. If also the second parameter, a globbing pattern, is passed, than it will
  be checked if the file, matching the globbing pattern, exists in the
  directory given by the first parameter.

#### Parameters
##### `path`  
A file path to check for existance or the base directory in which to
match the files against the glob-pattern.

##### `glob-pattern`  
_Attributes: optional, default: `#f`_  
The globbing pattern that must match
for the files in the directory given as the first parameter to return
true.


#### Returns
True if the file exists, false otherwise.

-------------------------------------------------
### resolve-path

#### Syntax
```scheme
(resolve-path path)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L413)

#### Export Name
`<import-name>/resolve-path`

#### Description
Resolves the given path to an absolute path, symlinks will alse be resolved.
This is achieved by calling the system command `readlink`.

#### Parameters
##### `path`  
The path to be resolved.


#### Returns
The resolved absolute path or false if failed.

-------------------------------------------------
### get-current-directory

#### Syntax
```scheme
(get-current-directory)
```

:arrow_right: [Go to source code of this procedure.](/src/file-system-operations.scm#L439)

#### Export Name
`<import-name>/get-current-directory`

#### Description
Retrieve the current directory.

#### Returns
The current directory or false if failed.

