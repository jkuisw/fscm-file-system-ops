:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: file-io.scm
Provides procedures for file input/output operations.

:arrow_right: [Go to source code of this file.](/src/file-io.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: read-line](#read-line) | Reads one line from a file and returns it as case sensitive string. |

## Procedure Documentation

### read-line

#### Syntax
```scheme
(read-line file-port)
```

:arrow_right: [Go to source code of this procedure.](/src/file-io.scm#L15)

#### Export Name
`<import-name>/read-line`

#### Description
Reads one line from a file and returns it as case sensitive string.

#### Parameters
##### `file-port`  
Port of the file to read, must be valid and open.


#### Returns
The read line or the eof object if reached end of file.

