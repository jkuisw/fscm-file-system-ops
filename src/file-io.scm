;*******************************************************************************
; Provides procedures for file input/output operations.

;*******************************************************************************
; Dependencies
; Nothing for now.

;*******************************************************************************
; Reads one line from a file and returns it as case sensitive string.
;
; Parameters:
;   file-port - Port of the file to read, must be valid and open.
;
; Returns: The read line or the eof object if reached end of file.
(define (read-line file-port) (let (
    (chars-list '())
  )
  ; Read one line from the file as character list.
  (set! chars-list (let read-chars-loop ((char (read-char file-port)))
    (cond
      ; Check if end of file.
      ((eof-object? char) char)
      ; Check if end of line.
      ((char=? char #\newline) '())
      ; Add current character to the character list and read the next character.
      (else (cons char (read-chars-loop (read-char file-port))))
    )
  ))

  ; Convert the character list to the proper return value.
  (cond
    ; If character list is empty, than there is an empty line, therefore return
    ; an empty string.
    ((null? chars-list) "")
    ; Check if reached end of file. If so, return eof object.
    ((eof-object? chars-list) chars-list)
    ; Convert character list to a string and return it.
    (else (list->string chars-list))
  )
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'read-line read-line)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
